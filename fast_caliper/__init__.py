# -*- coding: utf-8 -*-

"""Top-level package for fast-caliper."""

__author__ = """Olivier Davignon, and F.A.S.T"""
__email__ = 'fast-hep@cern.ch'
__version__ = '0.0.1'
