fast-caliper
=============
Optimizes cuts on a set of variables, to achieve optimal selection or binary categorization.

Installation
------------

Achieved through conda::

    bash
    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh -b -p $HOME/miniconda2 -u
    source ~/miniconda2/etc/profile.d/conda.sh
    conda create -n test_caliper
    conda activate test_caliper
    conda install python=3.7
    conda install pip
    git clone https://davignon@gitlab.cern.ch/fast-hep/prototype/fast-caliper.git fast_caliper
    cd fast_caliper ; pip install -e . ; cd ..
    git clone https://github.com/FAST-HEP/fast-plotter fast-plotter
    cd fast-plotter ; pip install -e . ; cd ..
    export PYTHONPATH="$PWD/fast_caliper:$PYTHONPATH"
    cd fast_caliper/

Usage
-----

1) To optimize cuts or categories::

    fast_caliper config/optimization_config.yaml

2) To combine several outputs from 1)::

    fast_caliper config/optimization_config.yaml --comb file1.csv,file2.csv,file3.csv

3) Example config file is::

    general:
        df_sig: test/tbl_signal.csv
        df_bkg: test/tbl_background.csv
        cut_on:
            - var1: ">"
            - var2: ">"
            - var3: "<"
        ignore:
            - category
            - var4
    name_content: weight_nominal:sumw
    #FOR figure_of_merit, OPTIONS ARE: asimov, asimov_uncertainty5pc, SoverSqrtB or SoverSqrtB_uncertainty5pc
    figure_of_merit: asimov_uncertainty5pc
    type: cut
    #type: category
    out_df_name: out.csv
